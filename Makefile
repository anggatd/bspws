OBJ := bspws.o lib/bspc.o lib/cJSON.o lib/convert.o
INC := -I./
WARNINGS := -pedantic -Wall -Wextra -Wno-implicit-int
CPPFLAGS += $(INC)
CFLAGS += -std=c99 $(WARNINGS)
LDLIBS := -lxcb

PREFIX ?= /usr/local
BINPREFIX ?= $(PREFIX)/bin

.PHONY: all debug install clean

all: bspws

debug: CFLAGS += -g -O0
debug: all

install: all
	mkdir -p "$(DESTDIR)$(BINPREFIX)"
	cp -p bspws "$(DESTDIR)$(BINPREFIX)"

clean:
	$(RM) bspws $(OBJ)

bspws: $(OBJ)
bspws.o: bspws.c
lib/bspc.o: lib/bspc.c lib/bspc.h
lib/cJSON.o: lib/cJSON.c lib/cJSON.h
lib/convert.o: lib/convert.c lib/convert.h
