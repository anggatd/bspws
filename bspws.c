/* bspws: reads `bspc subscribe report` and writes lemonbar output for
 * desktops, nodes, and related information
 */

#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "lib/bspc.h"
#include "lib/cJSON.h"
#include "lib/convert.h"

enum d_state {
	D_FOCUSED,
	D_UNFOCUSED,
};
enum d_occupancy {
	D_OCCUPIED,
	D_VACANT,
	D_URGENT,
};
struct str {
	size_t sz;
	char buf[BUFSIZ];
};

static const char* subscribe_q[] = {"subscribe", "report", ""};
static const char* focused_q[] = {"query", "--nodes",
	"--node", "focused", ""};
static const char* nodes_q[] = {"query", "--nodes",
	"--desktop", "focused",
	"--node", ".window", ""};
/* TODO: move some crap to config.h */
static const char* urgent_fg = "%{F#ff7c55}";
static const char* marked_bg = "%{B#4677cc}";
static const char* focused_fg = "%{F#0c0c0c}";
static const char* occupied_fg = "%{F#d4d4d4}";
static const char* unoccupied_fg = "%{F#777777}";
static const char* active_bg = "%{B#d4d4d4}";
static const char* focused_bg = "%{B#f48fd2}";
static const char* unfocused_bg = "%{B#0c0c0c}";
static const char* layout_pfx = "%{A:bspc desktop --layout next:} ";
static const char* layout_sfx = " %{A}";
static const char* node_pfx =
	"%{A4:bspc node --focus prev.!hidden.window:}"
	"%{A5:bspc node --focus next.!hidden.window:}";
static const char* node_sfx = "%{A}" "%{A}" "%{c}";
static const char* desktop_pfx =
	"%{A4:bspc desktop --focus prev.occupied:}"
	"%{A5:bspc desktop --focus next.occupied:}";
static const char* desktop_sfx = "%{A}" "%{A}" "%{r}" "\n";
static const char* layout_icon_monocle = "ﾓﾉｸﾙ";
static const char* layout_icon_tiled = "ﾀｲﾙ";
static const char* layout_icon = "?";

static struct str node_bar;
static struct str desktop_bar;
static desktops;
static unsigned focused_wid = 0x00000000;
static bool cur_monitor = false;
static bool show_inactive_nodes = true;
static bool show_inactive_desktops = true;

int main ();
static out (const char*);
static void proc_line (char*);
static void proc_item (char, const char*);
static void reset_bars (void);
static void draw_bars (void);
static void set_layout (char);
static unsigned get_wid (const char*);
static bool node_prop (const cJSON*, const char*);
static bool add_nodes (void);
static bool add_node (const char*);
static unsigned focused_node (void);
static void add_desktop (enum d_state, enum d_occupancy, const char*);
static void add_monitor (char, const char*);
static bool str_item (struct str*, const char*);
static void str_reset (struct str*);

int main ()
{
	int subscribe_fd = bspc_open(subscribe_q);
	char line[BUFSIZ] = "";

	if (subscribe_fd < 0)
		return EXIT_FAILURE;

	while (bspc_read(subscribe_fd, line, BUFSIZ - 1) > 0) {
		reset_bars();
		proc_line(line);
		add_nodes();
		draw_bars();
	}

	bspc_close(subscribe_fd);
	return EXIT_SUCCESS;
}

int out (const char* str)
{
	return fputs(str, stdout);
}

static char* get_last_line (char* line)
	/* bspc may return multiple \n-separated lines per read, but we only
	 * care about the latest line, so return ptr to that */
{
	char* ret = line;
	char* next_nl;

	while (( next_nl = strchr(ret, '\n') )) {
		if (next_nl[1] == '\0')
			return ret;
		else
			ret = next_nl + 1;
	}
	return ret;
}

/* side effect: converts ':' in *line to '\0' */
void proc_line (char* line)
{
	char* begin = get_last_line(line);
	char* end;

	if (!begin)
		return;
	do {
		end = strchr(begin, ':');
		if (end)
			*end = '\0';
		proc_item(begin[0], begin + 1);
		begin = end;
	} while (begin++);
}

void proc_item (char type, const char* value)
{
	switch (type) {
		case 'O': /* focused occupied desktop */
			add_desktop(D_FOCUSED, D_OCCUPIED, value);
			break;
		case 'o': /* unfocused occupied desktop */
			add_desktop(D_UNFOCUSED, D_OCCUPIED, value);
			break;
		case 'F': /* focused free desktop */
			add_desktop(D_FOCUSED, D_VACANT, value);
			break;
		case 'f': /* unfocused free desktop */
			add_desktop(D_UNFOCUSED, D_VACANT, value);
			break;
		case 'U': /* focused urgent desktop */
			add_desktop(D_FOCUSED, D_URGENT, value);
			break;
		case 'u': /* unfocused urgent desktop */
			add_desktop(D_UNFOCUSED, D_URGENT, value);
			break;

		case 'M': /* focused monitor */
		case 'm': /* unfocused monitor */
			add_monitor(type, value);
			break;

		case 'L': /* layout */
			if (cur_monitor)
				set_layout(value[0]);
			break;

		case 'T': /* state of focused node */
		case 'G': /* flags of focused node */
			break;
	}
}

void reset_bars (void)
{
	desktops = 0;
	str_reset(&node_bar);
	str_reset(&desktop_bar);
}

void draw_bars (void)
{
	/* add 1 to bar ptrs to remove leading whitespace */
	out(layout_pfx);
	out(layout_icon);
	out(layout_sfx);
	out(node_pfx);
	out(node_bar.sz? node_bar.buf + 1 : "");
	out(node_sfx);
	out(desktop_pfx);
	out(desktop_bar.sz? desktop_bar.buf + 1 : "");
	out(desktop_sfx);
	fflush(stdout);
}

void set_layout (char layout_id)
{
	if (!cur_monitor)
		return;
	switch (layout_id) {
		case 'M': /* monocle */
			layout_icon = layout_icon_monocle;
			break;
		case 'T': /* tiled */
			layout_icon = layout_icon_tiled;
			break;
	}
}

unsigned get_wid (const char* str)
{
	/* window IDs should be returned as hex, but this doesn't seem to
	 * be guaranteed by bspc(1) so just use automatic base
	 */
	long wid;

	errno = 0;
	wid = strtol(str, NULL, 0);
	if (errno) {
		perror("bspws focused_node");
		return 0;
	}
	if (wid > UINT_MAX
	 || wid < 0) {
		fprintf(stderr, "focused_node wid %li out of range\n", wid);
		return 0;
	}
	return (unsigned) wid;
}

void node_state (cJSON* client, struct str* node)
{
	cJSON* state = cJSON_GetObjectItemCaseSensitive(client, "state");
	char* state_icon = "?";

	if (!cJSON_IsString(state) || !state->valuestring)
		;
	else if (!strcmp(state->valuestring, "tiled"))
		state_icon = "t";
	else if (!strcmp(state->valuestring, "pseudo_tiled"))
		state_icon = "p";
	else if (!strcmp(state->valuestring, "floating"))
		state_icon = "f";
	else if (!strcmp(state->valuestring, "fullscreen"))
		state_icon = "s";
	str_item(node, state_icon);
}

bool node_prop (const cJSON* tree, const char* check)
{
	cJSON* elem = cJSON_GetObjectItemCaseSensitive(tree, check);

	return cJSON_IsTrue(elem);
}

bool add_nodes (void)
{
	int query_fd = bspc_open(nodes_q);
	char buf[BUFSIZ] = "";
	ssize_t sz;
	char* begin = buf;
	char* end;

	if (query_fd < 0)
		return false;
	sz = bspc_read(query_fd, buf, BUFSIZ - 1);
	bspc_close(query_fd);
	if (sz <= 0)
		return sz == 0;

	focused_wid = focused_node();

	do {
		end = strchr(begin, '\n');
		if (end)
			*end = '\0';
		add_node(begin);
		begin = end;
	} while (begin++);
	return true;
}

bool add_node (const char* wid_str)
{
	struct str node;
	const char* tree_q[5] = {"query", "--tree", "--node", wid_str, ""};
	unsigned wid = get_wid(wid_str);
	int query_fd = bspc_open(tree_q);
	char buf[BUFSIZ];
	ssize_t sz;
	cJSON* tree;
	cJSON* client;
	cJSON* urgent;
	bool focused;
	const char* fg;
	const char* bg;

	if (query_fd < 0)
		return false;

	sz = bspc_read(query_fd, buf, BUFSIZ - 1);
	bspc_close(query_fd);
	if (sz <= 0)
		return sz == 0;

	tree = cJSON_Parse(buf);
	if (!tree) {
		const char* err = cJSON_GetErrorPtr();
		fprintf(stderr, "cJSON_Parse: %s\n", err? err : "no error");
		cJSON_Delete(tree);
		return false;
	}

	client = cJSON_GetObjectItemCaseSensitive(tree, "client");
	if (!cJSON_IsObject(client)) {
		fprintf(stderr, "invalid json ('client' is not object)\n");
		return false;
	}

	focused = (wid == focused_wid);
	urgent = cJSON_GetObjectItemCaseSensitive(client, "urgent");
	fg = node_prop(tree, "hidden")? unoccupied_fg : occupied_fg;
	bg = node_prop(tree, "marked")? marked_bg : unfocused_bg;
	str_reset(&node);
	str_item(&node, " %{A: bspc node --focus ");
	if (focused) {
		fg = focused_fg;
		bg = focused_bg;
		str_item(&node, "last");
	}
	else {
		if (cJSON_IsTrue(urgent))
			fg = urgent_fg;
		str_item(&node, wid_str);
	}
	str_item(&node, ":}");
	str_item(&node, fg);
	str_item(&node, bg);
	str_item(&node, " ");

	node_state(client, &node);

	if (node_prop(tree, "sticky"))
		str_item(&node, "S");
	if (node_prop(tree, "private"))
		str_item(&node, "P");
	if (node_prop(tree, "locked"))
		str_item(&node, "L");
	if (show_inactive_nodes || focused) {
		cJSON* name = cJSON_GetObjectItemCaseSensitive(client,
		 "className");
		str_item(&node, ":");
		str_item(&node, (cJSON_IsString(name) && name->valuestring)?
		 name->valuestring : "?");
	}
	str_item(&node, " %{F-}" "%{B-}");
	str_item(&node_bar, node.buf);
	cJSON_Delete(tree);
	return true;
}

unsigned focused_node (void)
{
	int query_fd = bspc_open(focused_q);
	char buf[BUFSIZ];
	ssize_t sz;

	if (query_fd < 0)
		return false;

	sz = bspc_read(query_fd, buf, BUFSIZ - 1);
	bspc_close(query_fd);
	if (sz <= 0)
		return sz == 0;

	return get_wid(buf);
}

void add_desktop (enum d_state state, enum d_occupancy occupancy,
 const char* name)
{
	struct str desktop;
	char* desktops_str = ulltoa(++desktops);
	const char* fg = occupied_fg;
	const char* bg = unfocused_bg;

	str_reset(&desktop);
	str_item(&desktop, " %{A:bspc desktop --focus ");
	switch (state) {
		case D_FOCUSED:
			fg = focused_fg;
			bg = cur_monitor? focused_bg : active_bg;
			if (cur_monitor) {
				str_item(&desktop, "last:}");
				break;
			}
			/* else fallthrough */
		case D_UNFOCUSED:
			str_item(&desktop, "^");
			str_item(&desktop, desktops_str);
			str_item(&desktop, ":}");
			break;
	}
	switch (occupancy) {
		case D_URGENT:
			fg = urgent_fg;
			break;
		case D_OCCUPIED:
			break;
		case D_VACANT:
			fg = unoccupied_fg;
			break;
	}
	str_item(&desktop, fg);
	str_item(&desktop, bg);
	str_item(&desktop, " ");
	str_item(&desktop, desktops_str);
	if (show_inactive_desktops || state == D_FOCUSED) {
		str_item(&desktop, ":");
		str_item(&desktop, name);
	}
	str_item(&desktop, " %{F-}" "%{B-}" "%{-o}" "%{A}");
	str_item(&desktop_bar, desktop.buf);
}

void add_monitor (char type, const char* value)
{
	struct str monitor;

	cur_monitor = (type == 'M');
	str_reset(&monitor);
	str_item(&monitor, " ");
	str_item(&monitor, cur_monitor? occupied_fg : unoccupied_fg);
	str_item(&monitor, value);
	str_item(&desktop_bar, monitor.buf);
}

bool str_item (struct str* str, const char* item)
	/* add item to str unless it'd cut off */
{
	size_t sz = strlen(item);

	if (str->sz + sz >= BUFSIZ)
		return false;
	str->sz += sz;
	strcat(str->buf, item);
	return true;
}

void str_reset (struct str* str)
{
	str->sz = 0;
	str->buf[0] = '\0';
}
