/* Copyright (c) 2012, Bastien Dejean
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <xcb/xcb.h>

static char* SOCKET_PATH_TPL = "/tmp/bspwm%s_%i_%i-socket";
static char* SOCKET_ENV_VAR = "BSPWM_SOCKET";
static char FAILURE_MESSAGE = '\x07';

static bspc_connect (void)
{
	int sock_fd;
	struct sockaddr_un sock_address;
	char *sp;

	sock_address.sun_family = AF_UNIX;

	if ((sock_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("bspc socket");
		return -1;
	}

	sp = getenv(SOCKET_ENV_VAR);
	if (sp != NULL)
		snprintf(sock_address.sun_path, sizeof(sock_address.sun_path),
		 "%s", sp);
	else {
		char *host = NULL;
		int dn = 0;
		int sn = 0;

		if (xcb_parse_display(NULL, &host, &dn, &sn) != 0)
			snprintf(sock_address.sun_path,
			 sizeof sock_address.sun_path,
			 SOCKET_PATH_TPL, host, dn, sn);
		free(host);
	}

	if (connect(sock_fd, (struct sockaddr*) &sock_address,
	 sizeof sock_address) == -1) {
		perror("bspc connect");
		return -1;
	}
	return sock_fd;
}

int bspc_open (const char** query)
{
	int sock_fd = bspc_connect();
	char msg[BUFSIZ];
	size_t msg_len = 0;
	size_t rem = sizeof msg;
	int n = 0;

	if (sock_fd < 0)
		return -1;

	while (*query[0] && rem > 0) {
		n = snprintf(msg + msg_len, rem, "%s%c", *query, 0);
		if (n < 0) {
			perror("bspc snprintf");
			return -1;
		}
		msg_len += n;
		rem -= n;
		++query;
	}

	if (send(sock_fd, msg, msg_len, 0) == -1) {
		perror("bspc send");
		return -1;
	}

	return sock_fd;
}

ssize_t bspc_read (const sock_fd, char* rsp, size_t sz)
	/* TODO: should probably keep an internal dynamically-allocated
	 * buffer in case _for whatever reason_ sz is not large enough for
	 * a whole line. we can return entire lines as well in case one read
	 * returns multiple lines (can happen with `bspc subscribe` racing)
	 */
{
	struct pollfd fds[] = {
		{sock_fd, POLLIN, 0},
		{STDOUT_FILENO, POLLHUP, 0},
	};
	ssize_t nb;

	if (poll(fds, 2, -1) < 0) {
		perror("bspc poll");
		return -1;
	}

	if (fds[1].revents & (POLLERR | POLLHUP))
		return 0;
	if (!( fds[0].revents & POLLIN ))
		return 0;

	nb = recv(sock_fd, rsp, sz, 0);
	if (nb < 0)
		perror("bspc recv");
	else if (nb > 0
	 && rsp[nb-1] == '\n')
		rsp[nb-1] = '\0';

	rsp[nb] = '\0';
	if (rsp[0] == FAILURE_MESSAGE) {
		nb = -1;
		fprintf(stderr, "%s", rsp + 1);
		fflush(stderr);
	}

	return nb;
}

void bspc_close (const sock_fd)
{
	close(sock_fd);
}
