#include "lib/convert.h"
#define maxlen (sizeof "18446744073709551615")

char* ulltoa (unsigned long long val)
{
	static char buf[maxlen];
	char* ptr = buf + maxlen - 1;
	buf[maxlen-1] = '\0';
	while (val && --ptr >= buf) {
		*ptr = (val % 10) + '0';
		val /= 10;
	}
	return ptr;
}
